const user = (sequelize, DataTypes) => {
  const User = sequelize.define('user', {
    email: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false,
      validate: {
        is: /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g,
      },
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    roleId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      validate: {
        max: 5,
        min: 1,
      },
    },
    emergencyId: {
      type: DataTypes.STRING,
    },
    gender: {
      type: DataTypes.STRING,
    },
    phone: {
      type: DataTypes.STRING,
    },
    address: {
      type: DataTypes.STRING,
    },
    sessionId: {
      type: DataTypes.STRING,
      unique: true,
    },
    deactivated: {
      type: DataTypes.BOOLEAN,
    },
    creatorId: {
      type: DataTypes.INTEGER,
      references: {
        model: 'users',
        key: 'id',
      },
    },
    allergies: {
      type: DataTypes.TEXT,
      get: function () {
        return JSON.parse(this.getDataValue('allergies'));
      },
      set: function (val) {
        return this.setDataValue('allergies', JSON.stringify(val));
      },
    },
  });

  User.associate = models => {
    User.hasOne(models.User, { as: 'creator' });
  };

  User.beforeFind(options => {
    if (!options.attributes) options.attributes = {};
    options.attributes.exclude = [
      'createdAt',
      'updatedAt',
      'password',
      'sessionId',
    ];
    return options;
  });

  User.findBySessionId = async sessionId => {
    let user = await User.findAll({
      where: { sessionId },
    });

    return user;
  };

  return User;
};

export default user;
