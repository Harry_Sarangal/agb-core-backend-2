const prescription = (sequelize, DataTypes) => {
  const Prescription = sequelize.define('prescription', {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    information: {
      type: DataTypes.STRING,
    },
    refils: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    comments: {
      type: DataTypes.TEXT,
      get: function () {
        return JSON.parse(this.getDataValue('comments'));
      },
      set: function (val) {
        return this.setDataValue('comments', JSON.stringify(val));
      },
    },
  });

  Prescription.associate = models => {
    models.User.hasMany(Prescription, {
      foreignKey: {
        name: 'patientId',
      },
    });
  };

  return Prescription;
};

export default prescription;
