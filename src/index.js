import express from 'express';
import morgan from 'morgan';
import bodyParser from 'body-parser';
import cors from 'cors';
import authRoutes from './routes/auth';
import prescriptionRoutes from './routes/prescription';
import userRoutes from './routes/user';
import api from './utils/openapi';
import swaggerUi from 'swagger-ui-express';
import db from './config/database';

db.authenticate()
  .then(() => console.log('Database connected...'))
  .catch(err => console.log('Error DB => ', err));

const app = express();

app.use(cors());
app.use(morgan('dev'));
app.use(bodyParser.json());

app.use('/api', authRoutes);
app.use('/api', prescriptionRoutes);
app.use('/api', userRoutes);

app.get('/api-docs.json', function (req, res) {
  res.setHeader('Content-Type', 'application/json');
  res.send(api);
});

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(api));

module.exports = app;
