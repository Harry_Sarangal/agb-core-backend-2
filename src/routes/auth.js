import { Router } from 'express';

import { auth } from '../controllers';

const router = Router();

/**
 * @swagger
 *
 * tags:
 *   name: Auth
 *   description: Authentication endpoints
 */

/**
 * @swagger
 *
 * /login:
 *   post:
 *     produces:
 *       - application/json
 *     tags: [Auth]
 *     parameters:
 *       - name: email
 *         in: body
 *         required: true
 *         type: string
 *       - name: password
 *         in: body
 *         required: true
 *         type: string
 */
router.post('/login', auth.login);

/**
 * @swagger
 *
 * /logout:
 *   post:
 *     produces:
 *       - application/json
 *     tags: [Auth]
 *     parameters:
 *       - name: sessionId
 *         in: headers
 *         required: true
 *         type: string
 */
router.post('/logout', auth.logout);

/**
 * @swagger
 *
 * /me:
 *   post:
 *     produces:
 *       - application/json
 *     tags: [Auth]
 *     parameters:
 *       - name: sessionId
 *         in: headers
 *         required: true
 *         type: string
 */
router.post('/me', auth.checkUser);

/**
 * @swagger
 *
 * /password:
 *   put:
 *     produces:
 *       - application/json
 *     tags: [Auth]
 *     parameters:
 *       - name: sessionId
 *         in: headers
 *         required: true
 *         type: string
 *       - name: currentPassword
 *         in: body
 *         required: true
 *         type: string
 *       - name: newPassword
 *         in: body
 *         required: true
 *         type: string
 *       - name: newPasswordConfirm
 *         in: body
 *         required: true
 *         type: string
 */
router.put('/password', auth.changePassword);

module.exports = router;
