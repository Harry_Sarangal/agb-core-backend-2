import { Router } from 'express';

import { prescription } from '../controllers';

const router = Router();

/**
 * @swagger
 *
 * tags:
 *   name: Prescription
 *   description: Prescription management
 */

/**
 * @swagger
 *
 * /prescriptions:
 *   post:
 *     produces:
 *       - application/json
 *     tags: [Prescription]
 *     parameters:
 *       - name: sessionId
 *         in: headers
 *         required: true
 *         type: string
 *       - name: userId
 *         in: body
 *         required: true
 *         type: number
 *       - name: name
 *         in: body
 *         required: true
 *         type: string
 *       - name: refils
 *         in: body
 *         required: true
 *         type: number
 *       - name: information
 *         in: body
 *         required: false
 *         type: string
 */
router.post('/prescriptions', prescription.addPrescription);

/**
 * @swagger
 *
 * /prescriptions/:id:
 *   put:
 *     produces:
 *       - application/json
 *     tags: [Prescription]
 *     parameters:
 *       - name: sessionId
 *         in: headers
 *         required: true
 *         type: string
 *       - name: name
 *         in: body
 *         required: false
 *         type: string
 *       - name: refils
 *         in: body
 *         required: false
 *         type: number
 *       - name: information
 *         in: body
 *         required: false
 *         type: string
 *       - name: comments
 *         in: body
 *         required: false
 *         type: string
 */
router.put('/prescriptions/:prId', prescription.updatePrescriptionById);

export default router;
