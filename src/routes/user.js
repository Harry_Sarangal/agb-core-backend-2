import { Router } from 'express';

import { user } from '../controllers';

const router = Router();

/**
 * @swagger
 *
 * tags:
 *   name: User
 *   description: User management
 */

/**
 * @swagger
 *
 * /users:
 *   get:
 *     produces:
 *       - application/json
 *     tags: [User]
 *     parameters:
 *       - name: sessionId
 *         in: headers
 *         required: true
 *         type: string
 */
router.get('/users', user.getUsers);

/**
 * @swagger
 *
 * /users/:id:
 *   get:
 *     produces:
 *       - application/json
 *     tags: [User]
 *     parameters:
 *       - name: sessionId
 *         in: headers
 *         required: true
 *         type: string
 */
router.get('/users/:userId', user.getUserById);

/**
 * @swagger
 *
 * /users:
 *   post:
 *     produces:
 *       - application/json
 *     tags: [User]
 *     parameters:
 *       - name: sessionId
 *         in: headers
 *         required: true
 *         type: string
 *       - name: creatorId
 *         in: body
 *         required: true
 *         type: integer
 *       - name: email
 *         in: body
 *         required: true
 *         type: string
 *       - name: roleId
 *         in: body
 *         required: true
 *         type: enum (1-5)
 *       - name: password
 *         in: body
 *         required: true
 *         type: string
 *       - name: gender
 *         in: body
 *         required: false
 *         type: string
 *       - name: phone
 *         in: body
 *         required: false
 *         type: string
 *       - name: address
 *         in: body
 *         required: false
 *         type: string
 *       - name: deactivated
 *         in: body
 *         required: false
 *         type: boolean
 *       - name: allergies
 *         in: body
 *         required: false
 *         type: string[]
 */
router.post('/users', user.addUser);

/**
 * @swagger
 *
 * /users:
 *   put:
 *     produces:
 *       - application/json
 *     tags: [User]
 *     parameters:
 *       - name: sessionId
 *         in: headers
 *         required: true
 *         type: string
 *       - name: email
 *         in: body
 *         required: false
 *         type: string
 *       - name: gender
 *         in: body
 *         required: false
 *         type: string
 *       - name: phone
 *         in: body
 *         required: false
 *         type: string
 *       - name: address
 *         in: body
 *         required: false
 *         type: string
 *       - name: deactivated
 *         in: body
 *         required: false
 *         type: boolean
 *       - name: allergies
 *         in: body
 *         required: false
 *         type: string[]
 */
router.put('/users/:userId', user.updateUserById);

/**
 * @swagger
 *
 * /users/:id:
 *   delete:
 *     produces:
 *       - application/json
 *     tags: [User]
 *     parameters:
 *       - name: sessionId
 *         in: headers
 *         required: true
 *         type: string
 */
router.post('/users/:userId', user.deleteUser);

export default router;
