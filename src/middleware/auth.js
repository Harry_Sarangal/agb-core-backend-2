import models from '../models';

const auth = async (req, res, next) => {
  try {
    const users = await models.User.findAll({
      where: { sessionId: req.body.sessionId },
    });
    const user = users[0];

    if (!user || !req.body.sessionId || !user.email)
      return res.status(403).send({ error: 'Access denied' });

    if (!user || !req.body.sessionId || !user.email) {
      throw 'Access denied';
    } else {
      next();
    }
  } catch (error) {
    res.status(401).json({
      error: new Error('Invalid request!'),
    });
  }
};

export default auth;
