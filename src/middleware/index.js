import authMiddleware from './auth';

const middleware = { authMiddleware };

export default middleware;
