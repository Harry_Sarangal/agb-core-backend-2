import { addPrescription, updatePrescriptionById } from './prescription';
import models from '../models';

const mockRequest = (body, headers, params) => ({
  body,
  headers,
  params,
});

const mockResponse = (res = {}) => {
  res.status = jest.fn().mockReturnValue(res);
  res.json = jest.fn().mockReturnValue(res);
  res.send = jest.fn().mockReturnValue(res);
  return res;
};

describe('addPrescription', () => {
  test('should be 403 if no session id provided', async () => {
    const testBody = { sessionId: null };
    const req = mockRequest(testBody);
    const res = mockResponse();

    await addPrescription(req, res);

    expect(res.status).toHaveBeenCalledWith(403);
  });

  test('should be 200 if session id provided and user found', async () => {
    const testBody = { sessionId: 'test' };
    const req = mockRequest(testBody);
    const res = mockResponse();

    const usersSpy = jest.spyOn(models.User, 'findBySessionId');
    usersSpy.mockReturnValue([{}]);

    const prescriptionSpy = jest.spyOn(models.Prescription, 'create');
    prescriptionSpy.mockReturnValue({});

    await addPrescription(req, res);

    expect(res.send).toHaveBeenCalledWith({});
  });
});

describe('updatePrescriptionById', () => {
  test('update: should be 403 if no session id provided', async () => {
    const testHeaders = { sessionId: null };
    const req = mockRequest(undefined, testHeaders);
    const res = mockResponse();

    const usersSpy = jest.spyOn(models.User, 'findAll');
    usersSpy.mockReturnValue([]);

    await updatePrescriptionById(req, res);

    expect(res.status).toHaveBeenCalledWith(403);
  });

  test('should be 200 if session id provided and user found', async () => {
    const testHeaders = { sessionId: 'id' };
    const testBody = {
      information: 'test',
      save: () => {
        testBody.information = 'updated';
      },
    };
    const testParams = { prId: 1 };
    const req = mockRequest(testBody, testHeaders, testParams);
    const res = mockResponse();

    const usersSpy = jest.spyOn(models.User, 'findAll');
    usersSpy.mockReturnValue([{}]);

    const prescriptionSpy = jest.spyOn(models.Prescription, 'findAll');
    prescriptionSpy.mockReturnValue([testBody]);

    await updatePrescriptionById(req, res);
    expect(res.send).toHaveBeenCalledWith(testBody);
    expect(testBody.information).toMatch('updated');
  });
});
