import models from '../models';
import { Op } from 'sequelize';

const getUsers = async (req, res) => {
  try {
    if (req.headers.session_id) {
      const users = await models.User.findAll({
        where: { sessionId: req.headers.session_id },
        include: { model: models.Prescription, as: 'prescriptions' },
      });
      const user = users.length > 0 ? users[0] : false;

      if (!user) {
        res.status(403).send({ error: 'Access Denied.' });
      }

      let retrieveList;
      if (user) {
        switch (user.roleId) {
          case 1: {
            retrieveList = await models.User.findAll({
              where: { roleId: [1, 2, 3, 5] },
              include: { model: models.Prescription, as: 'prescriptions' },
            });
            break;
          }
          case 2: {
            retrieveList = await models.User.findAll({
              where: {
                roleId: 4,
                [Op.or]: [
                  { creatorId: user.id },
                  { emergencyId: { [Op.ne]: null } },
                ],
              },
              include: { model: models.Prescription, as: 'prescriptions' },
            });
            break;
          }
          case 5:
          case 3: {
            retrieveList = await models.User.findAll({
              where: { roleId: 4 },
              include: { model: models.Prescription, as: 'prescriptions' },
            });
            break;
          }
          case 4: {
            retrieveList = [user];
            break;
          }
          default: {
            break;
          }
        }

        res.send(retrieveList);
      }
    } else {
      res.status(403).send({ error: 'Access Denied.' });
    }
  } catch (error) {
    res.status(500).send(error);
  }
};

const getUserById = async (req, res) => {
  try {
    if (req.headers.session_id) {
      const users = await models.User.findAll({
        where: { sessionId: req.headers.session_id },
        include: { model: models.Prescription, as: 'prescriptions' },
      });
      const user = users.length > 0 ? users[0] : false;
      if (!user) {
        res.status(403).send({ error: 'Access Denied.' });
      }
      if (user) {
        const creatorQuery = user.roleId === 2 ? { creatorId: user.id } : {};
        const currentUsers = await models.User.findAll({
          where: { roleId: 4, id: Number(req.params.userId), ...creatorQuery },
          include: { model: models.Prescription, as: 'prescriptions' },
        });

        if (!currentUsers.length) {
          res.status(404).send({ error: 'User not found.' });
        }

        res.send(currentUsers[0]);
      }
    } else {
      res.status(403).send({ error: 'Access Denied.' });
    }
  } catch (error) {
    res.status(500).send(error);
  }
};

const addUser = async (req, res) => {
  if (!req.body.sessionId) {
    return res.status(403).send({ error: 'Access denied' });
  }

  let creator;
  try {
    creator = (await models.User.findBySessionId(req.body.sessionId))[0];

    if (!creator) {
      return res.status(403).send({ error: 'Access denied' });
    }
  } catch (error) {
    return res.status(500).send(error);
  }

  try {
    const user = await models.User.create({
      email: req.body.email,
      password: req.body.password,
      name: req.body.name,
      roleId: req.body.roleId,
      gender: req.body.gender,
      phone: req.body.phone,
      address: req.body.address,
      creatorId: creator.id,
    });

    res.send(user);
  } catch (error) {
    res.status(500).send(error);
  }
};

const deleteUser = async (req, res) => {
  try {
    const result = await models.User.destroy({
      where: {
        id: req.params.userId,
      },
    });
    res.send(true);
  } catch (error) {
    res.status(500).send(error);
  }
};

const updateUserById = async (req, res) => {
  try {
    let users = await models.User.findAll({
      where: { sessionId: req.headers.session_id },
    });
    let user = users.length > 0 ? users[0] : false;
    if (!user) res.status(403).send({ error: 'Access Denied.' });

    users = await models.User.findAll({
      where: { id: req.params.userId },
      include: { model: models.Prescription, as: 'prescriptions' },
    });
    user = users.length > 0 ? users[0] : false;
    if (!user) res.status(404).send({ error: 'Not found.' });

    const {
      email,
      name,
      roleId,
      gender,
      address,
      deactivated,
      emergencyId,
    } = req.body;
    user.email = typeof email !== 'undefined' ? email : user.email;
    user.name = typeof name !== 'undefined' ? name : user.name;
    user.roleId = typeof roleId !== 'undefined' ? roleId : user.roleId;
    user.gender = typeof gender !== 'undefined' ? gender : user.gender;
    user.address = typeof address !== 'undefined' ? address : user.address;
    user.deactivated =
      typeof deactivated !== 'undefined' ? deactivated : user.deactivated;
    user.emergencyId =
      typeof emergencyId != 'undefined' ? emergencyId : user.emergencyId;
    const saved = await user.save();
    res.send(user);
  } catch (error) {
    res.status(500).send(error);
  }
};

export { getUsers, getUserById, addUser, deleteUser, updateUserById };
