import models from '../models';
import crypto from 'crypto';

const generate_key = () => {
  // 16 bytes is likely to be more than enough,
  // but you may tweak it to your needs
  return crypto.randomBytes(16).toString('base64');
};

const login = async (req, res) => {
  try {
    const user = (
      await models.User.findAll({
        where: {
          email: req.body.email,
          password: req.body.password,
        },
        include: { model: models.Prescription, as: 'prescriptions' },
      })
    )[0];

    if (!user || user.deactivated)
      return res.status(403).send({ error: 'Access denied' });

    user.sessionId = generate_key();
    await user.save();

    res.status(200).send(user);
  } catch (error) {
    res.status(500).send(error);
  }
};

const logout = async (req, res) => {
  try {
    const users = await models.User.findAll({
      where: { sessionId: req.body.sessionId },
    });
    const user = users[0];

    user.sessionId = null;
    await user.save();

    res.send(true);
  } catch (error) {
    res.status(500).send(error);
  }
};

const changePassword = async (req, res) => {
  try {
    const users = await models.User.findAll({
      where: { sessionId: req.body.sessionId, password: req.body.oldPassword },
    });
    const user = users.length > 0 ? users[0] : null;
    if (user) {
      user.password = req.body.newPassword;
      await user.save();
      res.send(user);
    } else {
      res.status(403).send({ error: 'Access Denied.' });
    }
  } catch (error) {
    res.status(500).send(error);
  }
};

const checkUser = async (req, res) => {
  try {
    const users = await models.User.findAll({
      where: { sessionId: req.body.sessionId },
      include: { model: models.Prescription, as: 'prescriptions' },
    });
    const user = users[0];

    if (!user || !req.body.sessionId || !user.email)
      return res.status(403).send({ error: 'Access denied' });

    res.send(user);
  } catch (error) {
    res.status(500).send({ error: 'Internal error' });
  }
};

export { login, logout, checkUser, changePassword };
