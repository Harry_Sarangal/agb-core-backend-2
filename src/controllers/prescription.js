import models from '../models';

export const addPrescription = async (req, res) => {
  if (!req.body.sessionId) {
    return res.status(403).send({ error: 'Access denied' });
  }

  let creator;
  try {
    creator = (await models.User.findBySessionId(req.body.sessionId))[0];

    if (!creator) {
      return res.status(403).send({ error: 'Access denied' });
    }
  } catch (error) {
    return res.status(500).send(error);
  }

  try {
    const prescription = await models.Prescription.create({
      name: req.body.name,
      refils: req.body.refils,
      information: req.body.information,
      patientId: req.body.patientId,
    });

    res.send(prescription);
  } catch (error) {
    res.status(500).send(error);
  }
};

export const updatePrescriptionById = async (req, res) => {
  try {
    let users = await models.User.findAll({
      where: { sessionId: req.headers.session_id },
    });
    let user = users.length > 0 ? users[0] : false;
    if (!user) res.status(403).send({ error: 'Access Denied.' });

    const prescriptions = await models.Prescription.findAll({
      where: { id: req.params.prId },
    });
    const prescription = prescriptions.length > 0 ? prescriptions[0] : false;
    if (!prescription) res.status(404).send({ error: 'Not found.' });

    const { information, name, refils, comments } = req.body;

    prescription.information =
      typeof information !== 'undefined'
        ? information
        : prescription.information;
    prescription.name = typeof name !== 'undefined' ? name : prescription.name;
    prescription.comments =
      typeof comments !== 'undefined' ? comments : prescription.comments;
    prescription.refils =
      typeof refils !== 'undefined' ? refils : prescription.refils;

    await prescription.save();
    res.send(prescription);
  } catch (error) {
    res.status(500).send(error);
  }
};
