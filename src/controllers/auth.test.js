import { login, logout, checkUser, changePassword } from './auth';
import models from '../models';

const mockRequest = body => ({
  body,
});

const mockResponse = (res = {}) => {
  res.status = jest.fn().mockReturnValue(res);
  res.json = jest.fn().mockReturnValue(res);
  res.send = jest.fn().mockReturnValue(res);
  return res;
};

describe('login', () => {
  test('should be 403 if email is missing from body', async () => {
    const testUser = { password: 'password' };
    const req = mockRequest({}, testUser);
    const res = mockResponse();

    const spy = jest.spyOn(models.User, 'findAll');
    spy.mockReturnValue([]);

    await login(req, res);

    expect(res.status).toHaveBeenCalledWith(403);
  });
  test('should be 200 when correct credentials ', async () => {
    const testUser = {
      email: 'example@gmail.com',
      password: 'password',
      save: async () => {},
    };

    const req = mockRequest({}, testUser);
    const res = mockResponse();

    const spy = jest.spyOn(models.User, 'findAll');
    spy.mockReturnValue([testUser]);

    await login(req, res);

    expect(res.status).toHaveBeenCalledWith(200);
  });
});

describe('logout', () => {
  test('should return true', async () => {
    const testUser = {
      email: 'example@gmail.com',
      password: 'password',
      save: async () => {},
    };
    const req = mockRequest({ sessionId: 'session' });
    const res = mockResponse();

    const spy = jest.spyOn(models.User, 'findAll');
    spy.mockReturnValue([testUser]);

    await logout(req, res);
    expect(res.send).toHaveBeenCalledWith(true);
  });
});

describe('check user', () => {
  test('should return the user with the same session id', async () => {
    const testUser = { sessionId: 'asd' };
    const testUserToReturn = {
      sessionId: 'asd',
      name: 'Joe',
      email: 'mail@ru.ru',
    };
    const req = mockRequest(testUser);
    const res = mockResponse();

    const spy = jest.spyOn(models.User, 'findAll');
    spy.mockReturnValue([testUserToReturn]);

    await checkUser(req, res);
    expect(res.send).toHaveBeenCalledWith(testUserToReturn);
  });
  test('should be 403 if username with session id not found', async () => {
    const testUser = { sessionId: 'hello' };
    const req = mockRequest(testUser);
    const res = mockResponse();

    const spy = jest.spyOn(models.User, 'findAll');
    spy.mockReturnValue([]);

    await checkUser(req, res);
    expect(res.status).toHaveBeenCalledWith(403);
  });
});

describe('change password', () => {
  test('should be 403 if user not found data is not set', async () => {
    const testRequestBody = {
      sessionId: 'qwe',
      oldPassword: 'tata',
    };
    const req = mockRequest(testRequestBody);
    const res = mockResponse();
    await changePassword(req, res);
    expect(res.status).toHaveBeenCalledWith(403);
  });

  test('should return user if changed', async () => {
    const testRequestBody = {
      sessionId: 'qwe',
      email: 'example@gmail.com',
      oldPassword: 'password1',
    };
    const testUser = {
      sessionId: 'qwe',
      email: 'example@gmail.com',
      password: 'password',
      save: async () => {
        testUser.password = 'password1';
      },
    };
    const req = mockRequest(testRequestBody);
    const res = mockResponse([testUser]);

    const spy = jest.spyOn(models.User, 'findAll');
    spy.mockReturnValue([testUser]);

    await changePassword(req, res);

    expect(res.send).toHaveBeenCalledWith(testUser);
    const newPassword = res.send()[0].password;
    expect(newPassword).toMatch('password1');
  });
});
