import {
  getUsers,
  getUserById,
  addUser,
  deleteUser,
  updateUserById,
} from './user';
import models from '../models';

const mockRequest = (body, headers, params) => ({
  body,
  headers,
  params,
});

const mockResponse = (res = {}) => {
  res.status = jest.fn().mockReturnValue(res);
  res.json = jest.fn().mockReturnValue(res);
  res.send = jest.fn().mockReturnValue(res);
  return res;
};

describe('getUsers', () => {
  test('should be 403 if session id is incorrect in the request body', async () => {
    const testRequest = { session_id: 'session' };
    const req = mockRequest({}, testRequest);
    const res = mockResponse();

    const spy = jest.spyOn(models.User, 'findAll');
    spy.mockReturnValue([]);

    await getUsers(req, res);

    expect(res.status).toHaveBeenCalledWith(403);
  });

  test('should be able to get a list of users ', async () => {
    const testRequest = { session_id: 'session' };
    const retrieveList = [
      {
        email: 'cage@gmail.com',
        password: 'qa1ws',
        name: 'Nicolas Cage',
        roleId: 3,
        creatorId: 1,
        phone: '12345',
        address: '1 address',
        gender: 'Male',
      },
      {
        email: 'cage@gmail.com',
        password: 'qa1ws',
        name: 'Nicolas Cage',
        roleId: 3,
        creatorId: 1,
        phone: '12345',
        address: '2 address',
        gender: 'Male',
      },
    ];

    const req = mockRequest({}, testRequest);
    const res = mockResponse();

    const spy = jest.spyOn(models.User, 'findAll');
    spy.mockReturnValue(retrieveList);

    await getUsers(req, res);

    expect(res.send).toHaveBeenCalledWith(retrieveList);
  });
});

describe('getUserById', () => {
  test('should be 403 if session id is incorrect in the request body', async () => {
    const testRequest = { session_id: 'session' };
    const req = mockRequest({}, testRequest);
    const res = mockResponse();

    const spy = jest.spyOn(models.User, 'findAll');
    spy.mockReturnValue([]);

    await getUserById(req, res);

    expect(res.status).toHaveBeenCalledWith(403);
  });

  test('should provide a user record', async () => {
    const testRequest = { session_id: 'session' };
    const testParams = { userId: 1 };
    const testUser = [
      {
        email: 'cage@gmail.com',
        password: 'qa1ws',
        name: 'Nicolas Cage',
        roleId: 3,
        creatorId: 1,
        phone: '12345',
        address: '1 address',
        gender: 'Male',
      },
    ];

    const req = mockRequest({}, testRequest, testParams);
    const res = mockResponse();

    const spy = jest.spyOn(models.User, 'findAll');
    spy.mockReturnValue(testUser);

    await getUserById(req, res);

    expect(res.send).toHaveBeenCalledWith(testUser[0]);
  });
});

describe('addUser', () => {
  test('should be 403 if session id is not present in the body', async () => {
    const req = mockRequest({}, {}, {});
    const res = mockResponse();

    await addUser(req, res);

    expect(res.status).toHaveBeenCalledWith(403);
  });

  test('should be 403 if creator session is expired', async () => {
    const testRequest = { sessionId: 'session' };

    const req = mockRequest(testRequest, {}, {});
    const res = mockResponse();

    const spy = jest.spyOn(models.User, 'findBySessionId');
    spy.mockReturnValue([]);

    await addUser(req, res);

    expect(res.status).toHaveBeenCalledWith(403);
  });

  test('should add new user and return user object', async () => {
    const testRequestBody = {
      email: 'cage@gmail.com',
      password: 'qa1ws',
      name: 'Nicolas Cage',
      roleId: 3,
      creatorId: 1,
      phone: '12345',
      address: '1 address',
      gender: 'Male',
      sessionId: 'session',
    };
    const testUser = {
      email: 'cage@gmail.com',
      password: 'qa1ws',
      name: 'Nicolas Cage',
      roleId: 3,
      creatorId: 1,
      phone: '12345',
      address: '1 address',
      gender: 'Male',
    };

    const req = mockRequest(testRequestBody, {}, {});
    const res = mockResponse();

    const spy1 = jest.spyOn(models.User, 'findBySessionId');
    spy1.mockReturnValue([{ id: 1 }]);

    const spy2 = jest.spyOn(models.User, 'create');
    spy2.mockReturnValue(testUser);

    await addUser(req, res);

    expect(res.send).toHaveBeenCalledWith(testUser);
  });
});

describe('deleteUser', () => {
  test('should remove user', async () => {
    const testParams = { userId: 1 };

    const req = mockRequest({}, {}, testParams);
    const res = mockResponse();

    const spy = jest.spyOn(models.User, 'destroy');
    spy.mockReturnValue(true);

    await deleteUser(req, res);

    expect(res.send).toHaveBeenCalledWith(true);
  });
});

describe('updateUserById', () => {
  test('should be 403 if user not found data is not set ', async () => {
    const testHeader = { session_id: 'session' };

    const req = mockRequest({}, testHeader, {});
    const res = mockResponse();

    const spy = jest.spyOn(models.User, 'findAll');
    spy.mockReturnValue([]);

    await updateUserById(req, res);

    expect(res.status).toHaveBeenCalledWith(403);
  });

  test('should update the record', async () => {
    const testHeader = { session_id: 'session' };
    const testParams = { userId: 1 };
    const testRequestBody = {
      phone: '54321',
      address: '2 address',
    };
    const testUser = {
      phone: '12345',
      address: '1 address',
      save: async () => {
        testUser.phone = '54321';
        testUser.address = '2 address';
      },
    };
    const req = mockRequest(testRequestBody, testHeader, testParams);
    const res = mockResponse();

    const spy1 = jest.spyOn(models.User, 'findAll');
    spy1.mockReturnValue([testUser]);

    await updateUserById(req, res);

    expect(res.send).toHaveBeenCalledWith(testUser);
  });
});
