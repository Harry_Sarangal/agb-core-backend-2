# agb-core-backend

**Stack**:

- Node
- Express
- Nodemon
- Pg
- Sequelize
- Jest
- Supertest
- Eslint
- Prettier
- Docker
- Docker-compose

**Run**

1. `npm install`
2. `docker-compose up --build`
