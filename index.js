import server from './src';
import db from './src/config/database';
import models from './src/models';
import faker from 'faker';

const PORT = process.env.PORT || 5000;

/** ERASE DATABASE WHEN THE SERVER STARTS */
const eraseDatabaseOnSync = true;

db.sync({ force: eraseDatabaseOnSync }).then(async () => {
  if (eraseDatabaseOnSync) {
    createUsers();
  }
  server.listen(PORT, () => console.log(`server is running at ${PORT}`));
});

const genericPassword = 'qa1ws';

/** Example - SEED DATABASE USER (Only once) */
const createUsers = async () => {
  // admin
  await models.User.create({
    email: 'admin@drdaycare.ie',
    password: genericPassword,
    name: 'Holy Molly',
    roleId: 1,
    phone: faker.phone.phoneNumber(),
    address: faker.address.streetAddress(),
    gender: 'Female',
  });
  // doctor
  await models.User.create({
    email: 'doctor@drdaycare.ie',
    password: genericPassword,
    name: 'Harry',
    roleId: 2,
    creatorId: 1,
    phone: faker.phone.phoneNumber(),
    address: faker.address.streetAddress(),
    gender: 'Male',
  });
  await models.User.create({
    email: 'example@gmail.com',
    password: genericPassword,
    name: 'Ugo',
    roleId: 3,
    creatorId: 1,
    phone: faker.phone.phoneNumber(),
    address: faker.address.streetAddress(),
    gender: 'Male',
  });
  await models.User.create({
    email: 'example3@gmail.com',
    password: genericPassword,
    name: 'Sergei',
    roleId: 3,
    creatorId: 1,
    phone: faker.phone.phoneNumber(),
    address: faker.address.streetAddress(),
    gender: 'Male',
  });
  await models.User.create({
    email: 'cage@gmail.com',
    password: genericPassword,
    name: 'Nicolas Cage',
    roleId: 3,
    creatorId: 1,
    phone: faker.phone.phoneNumber(),
    address: faker.address.streetAddress(),
    gender: 'Male',
  });
  await models.User.create({
    email: 'example1@gmail.com',
    password: genericPassword,
    name: 'Esther',
    roleId: 4,
    creatorId: 2,
    phone: faker.phone.phoneNumber(),
    address: faker.address.streetAddress(),
    gender: 'Female',
    allergies: ['nuts', 'Adderall'],
  });

  await models.Prescription.create({
    refils: 4,
    information: 'Take 5 a day. 2 weeks.',
    name: 'Valium',
    patientId: 6,
    date: new Date().getTime(),
    comments: [
      'https://www.youtube.com/watch?v=17TbF6n4jI0',
      'Filler text is text that shares some characteristics of a real written text, but is random or otherwise generated. It may be used to display a sample of fonts, generate text for testing, or to spoof an e-mail spam filter. The process of using filler text is sometimes called greeking, although the text itself may be nonsense, or largely Latin, as in Lorem ipsum.',
      'Another comment',
      'Another comment',
      'Another comment',
      'Another one',
    ],
  });

  await models.Prescription.create({
    refils: 1,
    information: 'Take 1 a day. 4 weeks.',
    name: 'Vitamin C',
    patientId: 6,
    date: new Date().getTime(),
    comments: ['This is vitamin C'],
  });

  await models.Prescription.create({
    refils: 9,
    information: '2 spoons a day; mornings and evenings; 2 weeks.',
    name: 'Cough syrop',
    patientId: 6,
    date: new Date().getTime(),
  });

  await models.Prescription.create({
    refils: 1,
    information: 'Take 1 pill when in pain',
    name: 'Fentanyl',
    patientId: 6,
    date: new Date().getTime(),
  });

  await models.User.create({
    email: 'example2@gmail.com',
    password: genericPassword,
    name: 'Kerry',
    roleId: 4,
    creatorId: 2,
    phone: faker.phone.phoneNumber(),
    address: faker.address.streetAddress(),
    gender: 'Female',
  });

  await models.Prescription.create({
    refils: 4,
    information: 'Take 1 a day. 4 weeks.',
    name: 'Vitamin C',
    patientId: 7,
    date: new Date().getTime(),
  });

  await models.User.create({
    email: 'staff@drdaycare.ie',
    password: genericPassword,
    name: 'Homer',
    roleId: 5,
    creatorId: 2,
    phone: faker.phone.phoneNumber(),
    address: faker.address.streetAddress(),
    gender: 'Male',
  });

  let limit = 20;
  while (limit > 0) {
    await models.User.create({
      email: faker.internet.email(),
      name: faker.name.findName(),
      password: genericPassword,
      roleId: faker.random.number({ min: 1, max: 4 }),
      gender: 'Gender neutral',
      phone: faker.phone.phoneNumber(),
      address: faker.address.streetAddress(),
    });
    limit--;
  }
};
